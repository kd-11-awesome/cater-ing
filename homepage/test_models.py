from django.test import TestCase, Client
from django.urls import reverse
from homepage.models import Answer

class TestModels(TestCase):

    def setUp(self):
        new_answer = Answer.objects.create(key='pesan', answer='bisa hubungi kami di +62-812-9128-0150')

    def test_model_can_create_new_answer(self):
        self.assertEqual(Answer.objects.all().count(),1)
