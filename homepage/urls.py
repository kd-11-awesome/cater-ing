from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('qna/', views.qna, name='qna'),
    path('about-us/', views.about_us, name='about-us'),
    path('our-team/', views.our_team, name='our-team'),
    # dilanjutkan ...
]
