from django.shortcuts import render
from .models import Answer

# Create your views here.
def homepage(request):
    return render(request, 'Homepage.html')

def about_us(request):
    return render(request, 'About Us.html')

def our_team(request):
    return render(request, 'OurTeam-page.html')

def qna(request):
    if request.method == 'POST':
        splitted_keyword = request.POST.get('keyword').split(" ")
        answer = Answer.objects.all().get(key="notfound")
        for word in splitted_keyword:
            if len(Answer.objects.filter(key=word.lower()))==1:
                answer = Answer.objects.all().get(key=word)
        return render(request, 'QnA-page.html', {'answer' : answer})
    else:
        return render(request, 'QnA-page.html')
