[![pipeline status](https://gitlab.com/kd-11-awesome/cater-ing/badges/master/pipeline.svg)](https://gitlab.com/kd-11-awesome/cater-ing/commits/master)
[![coverage report](https://gitlab.com/kd-11-awesome/cater-ing/badges/master/pipeline.svg)](https://gitlab.com/kd-11-awesome/cater-ing/commits/master)

Aplikasi Cater-ing

**Nama-nama anggota kelompok**
1. Alia Junisar
2. Ferdinand Hanif Faozi
3. Mikhael Natalnael
4. Muhammad Jilham Luthfi

**Link herokuapp**

cater-ing.herokuapp.com

**Cerita aplikasi yang diajukan serta kebermanfaatannya**

Aplikasi Cater.ing adalah aplikasi pemesanan makanan sehat berbasis online. Aplikasi ini menyediakan beberapa menu makanan sehat dengan informasi singkat sesuai komponen dari menu tersebut. Terdapat beberapa kelebihan yang dapat ditawarkan Cater.ing kepada pelanggan :
Informatif
Aplikasi memudahkan pengguna mengetahui apa saja kandungan atau komposisi dari makanan yang ia pesan
High-mobility person friendly
Aplikasi juga memudahkan pengguna yang bermobilitas tinggi untuk memesan makanan yang sehat, namun tidak ingin merasa kesulitan memilih menu.

**Daftar fitur yang akan diimplementasikan**

1. Daftar Menu Makanan
Fitur ini menampilkan menu makanan sehat yang dapat dipesan oleh pelanggan. Sebelum mengisi di form pemesanan, pelanggan dapat melihat-lihat terlebih dahulu paket-paket yang tersedia

2. Form Pemesanan
Fitur ini berisi sebuah form yang meminta data dari pelanggan, misalnya berupa nama, umur, kontak yang bisa dihubungi, serta pilihan makanan yang dipilih

3. Artikel
Fitur ini berisi informasi seputar makanan dan gaya hidup sehat

4. Chatbot
Fitur ini merupakan pengganti dari customer service. Hal ini sebagai penerapan dari industri 4.0. Fitur ini akan menerima input dari user berupa angka (tentatif) dan chatbot akan merespon dengan memberikan jawaban sesuai pertanyaan yang diajukan.
Contoh pertanyaan:
- Bagaimana cara memesan paket makanan?
- Apa paket makanan yang cocok untuk kebutuhan saya?
- Apa saja jenis pembayaran yang dapat digunakan?


## [Live app on heroku](https://cater-ing.herokuapp.com)